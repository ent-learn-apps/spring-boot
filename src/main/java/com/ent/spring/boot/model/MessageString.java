package com.ent.spring.boot.model;

import lombok.Data;

@Data
public class MessageString {

    String message;

}
