package com.ent.spring.boot.service;

import com.ent.spring.boot.model.MessageString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class ProducerService {

    @Autowired
    KafkaTemplate kafkaTemplate;
    public String publishEvent(String message) {
        String topic ="ent-learning-events";

        //kafkaTemplate.send(topic,message);

        return "Event published success!";

    }
}
