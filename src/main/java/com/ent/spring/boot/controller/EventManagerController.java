package com.ent.spring.boot.controller;

import com.ent.spring.boot.model.MessageString;
import com.ent.spring.boot.service.ProducerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Method;

@RestController
public class EventManagerController {

    @Autowired
    private ProducerService producerService;


    @PostMapping("/publish")
    public String produceEvent(@RequestBody MessageString message){

        return producerService.publishEvent(message.getMessage());
    }

}
