FROM openjdk:8-jdk-alpine
MAINTAINER hemant
COPY build/libs/spring-boot-0.0.1.jar spring-boot-0.0.1.jar
ENTRYPOINT ["java","-jar","/spring-boot-0.0.1.jar"]